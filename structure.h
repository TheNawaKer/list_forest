#ifndef STRUCTURE
#define STRUCTURE
#include "list_forest.h"

typedef struct list_element_struct * list_element;

struct list_element_struct{
    list_element * next;
    void * element;
};

/*Définition de la structure list_forest */
typedef list_forest * list_forest_pt;

/*Définition de la structure list_forest_position
Cette struture est utilisée comme itérateur pour se déplacer dans les noeuds de la list_forest
*/
struct list_forest_position_struct
{
    list_forest_pt currentNode; /* pointeur sur le noeud courrant */
    list_element * currentElement; /* pointeur sur l'élément du noeud courrant */
    int init;
};


struct list_forest_struct
{
/* Attributs */
    list_forest_pt father; /* pointeur sur le père */
    list_forest_pt son; /* pointeur sur le fils */
    list_forest_pt brother; /* pointeur sur le frère */
    list_element * element; /* pointeur un élément*/
    void * value; /* pointeur sur la valeur */
/* Fonction */
    void ( * copy_value ) ( const void * value , void ** pt );
    /*utilisée pour copier une valeur (taille et type inconnus)*/
    void ( * free_value ) ( void ** );
    /*utilisée pour détruire une valeur (libére la mémoire concernée)*/
    void ( * copy_element ) ( const void * element , void ** pt );
    /*utilisée pour copier un élément (taille et type inconnus)*/
    void ( * free_element ) ( void ** );
    /*utilisée pour détruire un élément (libére la mémoire concernée)*/
};

int list_forest_position_has_father_root ( list_forest_position lfp);

#endif