/* FICHIER list_forest.c 
Créer par Jimmy Furet,Valentin Bouziat,Yoan Garnier
étudiant en L3 ingénierie informatique à l'Université d'Orléans

Date de création 13/10/2014
Date de modification 23/10/2014 
*/



#include <stdlib.h>
#include "list_forest.h"
#include "structure.h"

/* FONCTION list_forest_create 
Creation d'une list_forest générique
La liste créee est vide (pas de noeud,ni de racine)
Alloue la mémoire nécéssaire
*/
int list_forest_create ( list_forest * lf,
				void ( * copy_value ) ( const void * value , void ** pt ) ,
				void ( * free_value ) ( void ** ),
				void ( * copy_element ) ( const void * element , void ** pt ) ,
				void ( * free_element ) ( void ** ) ) {
	/* EST DE TYPE LIS_FOREST ?*/

	*lf = malloc(sizeof(struct list_forest_struct));

	(*lf)->father = NULL;
	(*lf)->son = NULL;
	(*lf)->brother = NULL;

	(*lf)->element = NULL;
	(*lf)->value = NULL;

	(*lf)->copy_value=copy_value;
	(*lf)->free_value=free_value;
	(*lf)->copy_element=copy_element;
	(*lf)->free_element=free_element;

return FOREST_LIST_OK;
}

/* FONCTION list_forest_destroy 
Détruit la list_forest et libère la mémoire allouée
Cette Fonction permet d'éviter les fuites mémoires
Utilise free_value et free_element.
*/
int list_forest_destroy ( list_forest * lf) {
/* EST DE TYPE LIS_FOREST ?*/

		list_element * tmp=NULL;
		while((*lf)->son!=NULL){
			list_forest_destroy((*lf)->son);
		}

		(*lf)->free_value(&((*lf)->value));

		while((*lf)->element!=NULL){
			(*lf)->free_element(&((*((*lf)->element))->element));
			tmp = (*((*lf)->element))->next;
			free((*((*lf)->element)));
			free((*lf)->element);
			(*lf)->element=tmp;
		}
		
		(*lf)->copy_value=NULL;
		(*lf)->free_value=NULL;
		(*lf)->copy_element=NULL;
		(*lf)->free_element=NULL;

		if((*lf)->father!=NULL){

			(*((*lf)->father))->son=(*lf)->brother;

			free((*lf)->father);
			free(*lf);
			free(lf);
		}else{
			free(*lf);
		}



	return FOREST_LIST_OK;
}

/* FONCTION list_forest_is_empty
Retourne TRUE si la list_forest passée en paramètre est vide
Sinon retourne FALSE
*/
int list_forest_is_empty ( list_forest lf) {
	/* EST DE TYPE LIS_FOREST ?*/


	if ((lf->father == NULL)&&
		(lf->son == NULL)&&
		(lf->brother == NULL)&&
		(lf->element == NULL)&&
		(lf->value == NULL)){
		return TRUE;
	}
	else {
		return FALSE;
	}

}

/* FONCTION list_forest_empty
Vide la list_forest passée en paramètre et supprime tous les noeuds
*/
int list_forest_empty ( list_forest lf) {
	if(lf->father!=NULL){
		list_forest_empty(*(lf->father));
	}else{
		list_forest_destroy(&lf);
	}
	return FOREST_LIST_OK;
}

/* FONCTION list_forest_position_create
Créer une nouvelle position sur le noeud courrant de la list_forest
*/
int list_forest_position_create ( list_forest lf,
					 list_forest_position *  lfp) {
	*lfp=malloc(sizeof(struct list_forest_position_struct));
	(*lfp)->currentNode=malloc(sizeof(list_forest));
	(*(*lfp)->currentNode)=&(*lf);
	(*lfp)->currentElement = lf->element;
	if(lf->element!=NULL){
		(*lfp)->init = TRUE;
	}else{
		(*lfp)->init = FALSE;
	}
	if(!list_forest_position_has_father(*lfp)){
		list_forest_position_left_son(*lfp);
	}
return FOREST_LIST_OK;
}

/* FONCTION list_forest_position_duplicate
Créer une copie lfp2 de la list_forest_position lfp passée en paramètre
*/
int list_forest_position_duplicate ( list_forest_position lfp,
					    list_forest_position * lfp2) {

	list_forest_position_create((*(lfp)->currentNode),lfp2);
	(*lfp2)->currentElement = (lfp)->currentElement;
	(*lfp2)->init = (lfp)->init;
return 0;
}

/* FONCTION list_forest_position_destroy 
Détruit la list_forest_position passée en paramètre
Libère la mémoire allouée
Cette Fonction permet d'éviter les fuites mémoires
*/
int list_forest_position_destroy ( list_forest_position * lfp) {

	*((*lfp)->currentNode)=NULL;
	free((*lfp)->currentNode);
	(*lfp)->currentNode=NULL;
	(*lfp)->currentElement=NULL;
	free(*lfp);
	(*lfp)=NULL;
return 0;
}

/* FONCTION list_forest_position_has_father_root
Retourne TRUE si la list_forest_position pointe sur un noeud qui a la racine pour pere
Sinon retourne FALSE
*/
int list_forest_position_has_father_root ( list_forest_position lfp) {
	if((*((*(lfp->currentNode))->father))->father==NULL){
		return TRUE;
	}else{
		return FALSE;
	}
}

/* FONCTION list_forest_position_has_father
Retourne TRUE si la list_forest_position pointe sur un noeud qui a un père
Sinon retourne FALSE
(Seule la racine n'a pas de père.)
*/
int list_forest_position_has_father ( list_forest_position lfp) {
	if((*(lfp->currentNode))->father!=NULL){
		return TRUE;
	}else{
		return FALSE;
	}
}

/* FONCTION list_forest_position_father
Fait pointer la list_forest_position sur le père de son noeud courrant(si il en a un)
Met à jour l'élément
*/
int list_forest_position_father ( list_forest_position lfp) {
	if(list_forest_position_has_father(lfp)){
		if((*((*(lfp->currentNode))->father))->father!=NULL){
			*(lfp->currentNode)=*((*(lfp->currentNode))->father);
			lfp->currentElement=(*(lfp->currentNode))->element;
			lfp->init = (*(lfp->currentNode))->element!=NULL;
		}
	}
return 0;
}

/* FONCTION list_forest_position_has_left_son
Retourne TRUE si la list_forest_position pointe sur un noeud qui a un fils
Sinon retourne FALSE
*/
int list_forest_position_has_left_son ( list_forest_position lfp) {
	if((*(lfp->currentNode))->son!=NULL){
		return TRUE;
	}else{
		return FALSE;
	}
}

/* FONCTION list_forest_position_left_son
Fait pointer la list_forest_position sur le premier fils de son noeud courrant.(si il en a un )
Met à jour l'élément
*/
int list_forest_position_left_son ( list_forest_position lfp) {
	if(list_forest_position_has_left_son(lfp)){
		*(lfp->currentNode)=*((*(lfp->currentNode))->son);
		lfp->currentElement=(*(lfp->currentNode))->element;
		lfp->init = (*(lfp->currentNode))->element!=NULL;
	}
return 0;
}

/* FONCTION list_forest_has_next_brother
Retourne TRUE si la list_forest_position pointe sur un noeud qui a un frère
Sinon retourne FALSE
*/
int list_forest_position_has_next_brother ( list_forest_position lfp) {
if((*(lfp->currentNode))->brother!=NULL){
		return TRUE;
	}else{
		return FALSE;
	}
}

/* FONCTION list_forest_position_next_brother
Fait pointer la list_forest_position sur le frère de son noeud courrant.(si il en a un)
Met à jour l'élément
*/
int list_forest_position_next_brother ( list_forest_position lfp) {

if(list_forest_position_has_next_brother(lfp)){
		(*lfp->currentNode)=*((*(lfp->currentNode))->brother);
		lfp->currentElement=(*(lfp->currentNode))->element;
		lfp->init = (*(lfp->currentNode))->element!=NULL;
}else if (!list_forest_position_has_father(lfp)){
	list_forest_position_left_son(lfp);
}

return 0;
}

/* FONCTION list_forest_positon_value
Retourne l'adresse de la valeur
Aucune copie n'est faite, on renvoie l'adresse en brut
*/
void * list_forest_position_value ( list_forest_position lfp) {
 return (*(lfp->currentNode))->value;
}


/* FONCTIONS DE MODIFICATION  */

/* FONCTION list_forest_add_next_brother
Créer un frère au noeud courrant
Créer la racine si lfp est vide
 */
int list_forest_add_next_brother ( list_forest_position lfp,
					  const void * value ) {


	if(list_forest_is_empty((*lfp->currentNode))){
		list_forest_add_left_son(lfp,value);
	}else if(list_forest_position_has_father(lfp)){

		list_forest newBrother;

		list_forest_create(&newBrother,
			(*(lfp->currentNode))->copy_value,
			(*(lfp->currentNode))->free_value,
			(*(lfp->currentNode))->copy_element,
			(*(lfp->currentNode))->free_element);

			newBrother->father=malloc(sizeof(list_forest));
			*(newBrother->father)=*((*(lfp->currentNode))->father);/*le frere pointe sur le pere*/
			if(list_forest_position_has_next_brother(lfp)){
				list_forest_pt brother=(*(lfp->currentNode))->brother;
				(*(lfp->currentNode))->brother=malloc(sizeof(list_forest));
				*((*(lfp->currentNode))->brother)=&(*newBrother);/*le dernier frere pointe sur le nouveau frere*/
				newBrother->brother=brother;

			}else{
				(*(lfp->currentNode))->brother=malloc(sizeof(list_forest));
				*((*(lfp->currentNode))->brother)=&(*newBrother);/*le dernier frere pointe sur le nouveau frere*/
			}

			newBrother->copy_value(value,&(newBrother->value));/*copy value*/
	}else{
		/*ERROR*/
	}

return 0;
}

/* FONCTION list_forest_add_left_son
Créer un fils au noeud courrant
Créer la racine si lfp est vide
 */
int list_forest_add_left_son ( list_forest_position lfp,
				      const void * value ) {

	list_forest newSon;
	list_forest_pt exSon;
	list_forest_create(&newSon,
	(*(lfp->currentNode))->copy_value,
	(*(lfp->currentNode))->free_value,
	(*(lfp->currentNode))->copy_element,
	(*(lfp->currentNode))->free_element);

	exSon = (*(lfp->currentNode))->son;

	newSon->father=malloc(sizeof(list_forest));
	*(newSon->father)=*(lfp->currentNode);/*le fils pointe sur le pere*/
	(*(lfp->currentNode))->son=malloc(sizeof(list_forest));
	(*((*(lfp->currentNode))->son))=&(*newSon);/*le pere pointe sur le fils*/
	newSon->copy_value(value,&(newSon->value));
	newSon->brother=exSon;

return 0;
}

/* FONCTION list_forest_position_left_son_remove
Détruit le fils du noeuds courrant
Fait appel à la fonction list_forest_destroy
*/
int list_forest_position_left_son_remove ( list_forest lf,
						  list_forest_position lfp) {
if(!list_forest_is_empty(lf)){
	if(list_forest_position_has_left_son(lfp)){
		list_forest_destroy(((*(lfp->currentNode))->son));
	}
}
return 0;
}

/* FONCTION list_forest_position_left_son_remove
Détruit le frère du noeuds courrant
Fait appel à la fonction list_forest_destroy
*/
int list_forest_position_next_brother_remove ( list_forest lf,
						      list_forest_position lfp) {
if(!list_forest_is_empty(lf)){
	if(list_forest_position_has_next_brother(lfp)){
		list_forest tmp;
		tmp=*((*(lfp->currentNode))->brother);
		free((*(lfp->currentNode))->brother);
		(*(lfp->currentNode))->brother=tmp->brother;
		free(tmp->father);
		tmp->father=NULL;
		list_forest_destroy(&tmp);
	}
}
return 0;
}

/* DEPLACEMENTS DANS LA LISTE CHAINEE */

/* FONCTION list_forest_position_has_element
Retourne TRUE si lfp possède un élément
Sinon retourne FALSE
*/
int list_forest_position_has_element ( list_forest_position lfp) {
	if(!lfp->init){
		lfp->currentElement = (*(lfp->currentNode))->element;
	}
	if(lfp->currentElement!=NULL){
		lfp->init=TRUE;
		return TRUE;
	}else{
		return FALSE;

	}
}

/** Move onto the next element (or out of the list if it was the last element). **/
int list_forest_position_element_next ( list_forest_position lfp) {
	if(lfp->currentElement!=NULL){
		lfp->currentElement=(*(lfp->currentElement))->next;
	}
return 0;
}

/* FONCTION list_forest_position_element
Retourne l'élément courrant de lfp
*/
void * list_forest_position_element ( list_forest_position lfp) {
	if(list_forest_position_has_element(lfp)){
		return (*(lfp->currentElement))->element;
	}
	return NULL;
}


/* MODIFICATIONS DE LA LISTE CHAINEE */

/** Before curent element (if any).
    Uses copy_element **/
int list_forest_add_element ( list_forest_position lfp,
				     const void * element ) {

	list_element tmp;
	if(list_forest_position_has_element(lfp)){
		tmp=*(lfp->currentElement);
		*(lfp->currentElement)=malloc(sizeof(struct list_element_struct));
		(*(lfp->currentElement))->next=malloc(sizeof(list_element));
		*((*(lfp->currentElement))->next)=tmp;
	}else{
		if((*(lfp->currentNode))->element!=NULL){
			if(lfp->init){
				tmp=*((*(lfp->currentNode))->element);
				while(tmp->next!=NULL){
					tmp=*(tmp->next);
				}
				tmp->next=malloc(sizeof(list_element));
				*(tmp->next)=malloc(sizeof(struct list_element_struct));
				(*(tmp->next))->next=NULL;
				lfp->currentElement=tmp->next;
			}else{
				lfp->currentElement=(*(lfp->currentNode))->element;
				lfp->init=TRUE;
				list_forest_add_element(lfp,element);
			}
		}else{
			(*(lfp->currentNode))->element=malloc(sizeof(list_element));
			lfp->currentElement=(*(lfp->currentNode))->element;
			*(lfp->currentElement)=malloc(sizeof(struct list_element_struct));
			(*(lfp->currentElement))->next=NULL;
			lfp->init=TRUE;
		}
	}
	(*(lfp->currentNode))->copy_element(element,&((*(lfp->currentElement))->element));
return 0;
}

/** Printing.
    Comes handy when debugging. **/
void list_forest_fprint ( FILE * file,
				 list_forest lf,
				 void ( * print_value ) ( FILE * , void * ) ,
				 void ( * print_element ) ( FILE * , void * ) ){
/* EST DE TYPE LIS_FOREST ?*/
/* EST VIDE ? */
	static int pos=0;
	int posmp=pos;
	list_element * tmp=NULL;
	if(lf->father==NULL && lf->son!=NULL)
		list_forest_fprint(file,(*lf->son),print_value,print_element);
	else if(!list_forest_is_empty(lf)){
		int i;
		for(i=0;i<pos;i++){
			fprintf(file, " ");
		}
		if(lf->value!=NULL){
			print_value(file,lf->value);				
		}
		fprintf(file, " ");
		tmp=lf->element;
		fprintf(file, "[ ");
		while(tmp!=NULL){
			print_element(file,(*tmp)->element);
			fprintf(file, " ");
			tmp=(*tmp)->next;
		}
		fprintf(file, "]\n");
		if(lf->son!=NULL){
			pos++;
			list_forest_fprint(file,*(lf->son),print_value,print_element);
		}
		if(lf->brother!=NULL){
			pos=posmp;
			list_forest_fprint(file,*(lf->brother),print_value,print_element);
		}
	}

}
