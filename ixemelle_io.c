#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ixemelle_io.h"
#include "structure.h"
#define TAILLETAG 2

/* Compile Mais read_key er read_value ne modifient pas la foret */
/* Boucle infinie quand il y a des attributs */

int read_key(FILE * input,attribute attribut);
int read_value (FILE * input,attribute attribut);
int read_attribute(FILE * input,list_forest_position * lfp);
int read_tag (FILE * input,list_forest_position * lfp);
int read_name (FILE * input,char name[]);
void toLower(char* value);


static void xml_fprint_value ( FILE * f ,
					    void * pt ) {
  fprintf ( f , "%s" , (char*)pt ) ;
}

static void xml_fprint_element ( FILE * f ,
					      void * pt ) {
  fprintf ( f , "%s=\"%s\"" ,((attribute)pt)->key,((attribute)pt)->value) ;
}

int read_tag (FILE * input,list_forest_position * lfp){
	char name[NAME_MAX_LENGTH];
	char c;
	fpos_t position;
	int brother=FALSE;
	do{
		c=fgetc(input);
		memset(&name[0], 0, sizeof(name));
		if(c=='<'){
			fgetpos(input,&position);
			c=fgetc(input);
			if(c=='/'){
				read_name(input,name);
				c=fgetc(input);
				if(c=='>'){
					if(list_forest_position_has_father_root(*lfp)){
						brother=TRUE;
					}else{
						list_forest_position_father(*lfp);
					}
					
				}
			}else{
				fsetpos(input,&position);
				read_name(input,name);
				toLower(name);
				if(brother){
					list_forest_add_next_brother(*lfp,name);/* enregistrer dans la list_forest */
					list_forest_position_next_brother(*lfp);
					brother=FALSE;
				}else{
					if(list_forest_position_has_left_son(*lfp)){
						list_forest_position_left_son(*lfp);
						while(list_forest_position_has_next_brother(*lfp)){
							list_forest_position_next_brother(*lfp);
						}
						list_forest_add_next_brother(*lfp,name);
						list_forest_position_next_brother(*lfp);
					}else{
						list_forest_add_left_son(*lfp,name);/* enregistrer dans la list_forest */
						list_forest_position_left_son(*lfp);
					}
					
				}

				fgetpos(input,&position);
				c=fgetc(input);
				if (c==' '){
					read_attribute(input,lfp);
				}else{
					fsetpos(input,&position);
				}
			}
		}else if(c=='>'){
			if(brother){
				list_forest_position_next_brother(*lfp);
			}else{
				list_forest_position_left_son(*lfp);
			}
				/* si erreur mauvaise ">" */
		}else if(c=='/'){
			c=fgetc(input);
			if(c=='>'){
				if(list_forest_position_has_father_root(*lfp)){
					brother=TRUE;
				}else{
					list_forest_position_father(*lfp);
				}
			}
		}else if(c!=' ' && c!='\n'){
			break;
		}
	}while(c!=EOF);
	return XMLIGHT_SYNTAX_OK;
}

int read_name (FILE * input,char name[]){
int i;
char c;
fpos_t position;
for(i=0;i<=NAME_MAX_LENGTH;i++){
	fgetpos(input,&position);
	c=fgetc(input);
	if(c==EOF){  /* cas d'erreur EOF */
		return XMLIGHT_SYNTAX_ERROR_NAME_END;
	} 
	else if ((c>='A' && c<='Z') || (c>='a' && c<='z')){/* pas d'erreurs de syntaxe */
   		if (i==NAME_MAX_LENGTH){/* nom trop long */
			return XMLIGHT_SYNTAX_ERROR_NAME_TOO_LONG;
		}else{
			name[i]=c;
		}
	} 
	else if (c=='>' || c==' ' || c=='/'){
		fsetpos(input,&position);
		if(strlen(name)==0){
			return XMLIGHT_SYNTAX_ERROR_NAME_EMPTY;
		}else{
			break;
		}
	}else{
		return XMLIGHT_SYNTAX_ERROR;
	}
}
return XMLIGHT_SYNTAX_OK;
}

/* FONCTION read_key */
int read_key (FILE * input,attribute attribut){
	char key[KEY_MAX_LENGTH+1];
	int i;
	char c;
	memset(&key[0], 0, sizeof(key));
	for(i=0;i<=KEY_MAX_LENGTH;i++){
		c=fgetc(input);
		if (c==EOF){  /* cas d'erreur EOF */
			return XMLIGHT_SYNTAX_ERROR_KEY_END;
		} 

		else if ((c>='A' && c<='Z') || (c>='a' && c<='z')){/* pas d'erreurs de syntaxe */
			if (i==KEY_MAX_LENGTH){/* clé trop longue */
				return XMLIGHT_SYNTAX_ERROR_KEY_TOO_LONG;
			}else{
				key[i]=c;
			}
		}	
		else if (c=='='){
			if(strlen(key)==0){
				return XMLIGHT_SYNTAX_ERROR_KEY_EMPTY;
			}else{
				key[strlen(key)]='\0';
				break;
			}
		}else{
			return XMLIGHT_SYNTAX_ERROR;
		}

	}
	toLower(key);
	strcpy(attribut->key, key);
	
	return XMLIGHT_SYNTAX_OK;

}

/* FONCTION read_value */
int read_value (FILE * input,attribute attribut){
	char value[VALUE_MAX_LENGTH+1];
	int i;
	char c;
	memset(&value[0], 0, sizeof(value));
	c=fgetc(input);/* lecture du premier caractère */
	if (c=='"'){
		for(i=0;i<=VALUE_MAX_LENGTH;i++){
			c=fgetc(input);
			if (c==EOF){  /* cas d'erreur EOF */
				return XMLIGHT_SYNTAX_ERROR_VALUE_END;
			} 

			else if ((c>='A' && c<='Z') || 
					 (c>='a' && c<='z') || 
					 (c>= '0' && c<= '9')){/* pas d'erreurs de syntaxe */
				if (i==VALUE_MAX_LENGTH){/* valeur trop longue */
					return XMLIGHT_SYNTAX_ERROR_VALUE_TOO_LONG;
				}else{
					value[i]=c;
				}
			}	
			else if (c=='"'){
				if(strlen(value)==0){
					return XMLIGHT_SYNTAX_ERROR_VALUE_EMPTY;
				}else{
					value[strlen(value)]='\0';
					break;
				}
			}else{
				return XMLIGHT_SYNTAX_ERROR;
			}

		}
		strcpy(attribut->value, value);
		return XMLIGHT_SYNTAX_OK;

	}else{
		return XMLIGHT_SYNTAX_ERROR_VALUE_START;
	}

}
/* FONCTION read_attribute */
/* Utilise les fonctions read_key et read_name jusqu'a une balise de fin */
int read_attribute(FILE * input,list_forest_position * lfp){
	char c=NULL;
	fpos_t position;
	while (c!='>' && c!='/'){
		attribute_struct attribut;
		read_key(input,&attribut);
		read_value(input,&attribut);
		list_forest_add_element(*lfp,&attribut);
		list_forest_position_element_next(*lfp);
		fgetpos(input,&position);
		c=fgetc(input);
	}
	fsetpos(input,&position);
return XMLIGHT_SYNTAX_OK;
}

void toLower(char value[])
{
    int i;
    int length = strlen(value);
    for(i = 0; i < length ; i++) {
        value[i] = tolower(value[i]);
    }
}


/** Read an xmlight source from the file input.
    The data read is stored in newly created list_forest that will be recorded in al.
    int return error value or XMLIGHT_SYNTAX_OK if OK.
**/
int ixemelle_fscan ( FILE * input , list_forest * lf){
	list_forest_position lfp;
	int err=0;
	list_forest_position_create(*lf,&lfp);
	err = read_tag(input,&lfp);
	list_forest_position_destroy(&lfp);
	return err;
}




/** xmlight output onto the FILE. 
 Each time there is non son, the  < ... /> form is used. **/ 
void ixemelle_fprint ( FILE * output , list_forest lf){
	static int pos=0;
	int posmp=pos;
	list_element * tmp=NULL;
	if(lf->father==NULL && lf->son!=NULL)
		ixemelle_fprint(output,(*lf->son));
	else if(!list_forest_is_empty(lf)){
		int i;
		for(i=0;i<pos;i++){
			fprintf(output, " ");
		}
		fprintf(output, "<");
		if(lf->value!=NULL){
			xml_fprint_value(output,lf->value);				
		}
		tmp=lf->element;
		while(tmp!=NULL){
			fprintf(output, " ");
			xml_fprint_element(output,(*tmp)->element);
			tmp=(*tmp)->next;
		}
		if(lf->son!=NULL){
			fprintf(output, ">\n");
			pos++;
			ixemelle_fprint(output,*(lf->son));
			pos--;
			for(i=0;i<pos;i++){
				fprintf(output, " ");
			}
			fprintf(output, "</");
			if(lf->value!=NULL){
				xml_fprint_value(output,lf->value);				
			}
			fprintf(output, ">\n");
		}else{
			fprintf(output, "/>\n");
		}
		if(lf->brother!=NULL){
			pos=posmp;
			ixemelle_fprint(output,*(lf->brother));
		}
	}
}
