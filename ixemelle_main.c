# include <stdlib.h>   /* malloc */
# include <string.h>   /* memcpy */
# include <stdio.h>    /* printf */
#include "ixemelle_io.h"
#include "structure.h"

static void xml_copy_value ( const void * value , void ** pt ) {
  * pt = NULL ;
  * pt = (void *) malloc ((sizeof(char))*15);
  memcpy ( * pt , value , (sizeof(char))*15);
}

static void xml_copy_element ( const void * value , void ** pt ) {
  * pt = malloc ( sizeof ( attribute_struct ) ) ;
  memcpy ( * pt , value , sizeof (attribute_struct) );
}


static void xml_free_value ( void ** pt ) {
  free ( * pt ) ;
  * pt = NULL ;
}

static void xml_free_element ( void ** pt ) {
  free ( * pt ) ;
  * pt = NULL ;
}

int main(int argc, char** argv){
	FILE * file;
	list_forest lf;
	if(argc>1){
		file = fopen(argv[1],"r");
		if(file!=NULL){
			list_forest_create(&lf,&xml_copy_value,&xml_free_value,&xml_copy_element,&xml_free_element);
			ixemelle_fscan(file,&lf);
			fclose(file);
			ixemelle_fprint(stdout,lf);
			list_forest_destroy(&lf);
		}else{
			printf  ("%s" , "File Error" );
		}
	}else{
		printf ("%s", "Pas de fichier");
	}
	return 0;
}
